class TweetsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @tweets = Tweet.includes(:user).all.order(id: :desc).page(page)
  end

  private

  def page
    params[:page].to_i rescue 1
  end
end
