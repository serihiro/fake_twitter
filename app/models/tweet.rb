# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  tweet      :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_tweets_on_user_id  (user_id)
#

class Tweet < ApplicationRecord
  validates :tweet, presence: true, length: { maximum: 140 }
  validates :user_id, presence: true

  belongs_to :user
  has_many :tweet_favorites
  has_many :favorited_by, foreign_key: :favorited_by_user_id
end
