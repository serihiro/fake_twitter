# == Schema Information
#
# Table name: tweet_favorites
#
#  id                   :integer          not null, primary key
#  tweet_id             :integer          not null
#  favorited_by_user_id :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_tweet_favorites_on_favorited_by_user_id  (favorited_by_user_id)
#  index_tweet_favorites_on_tweet_id              (tweet_id)
#

class TweetFavorite < ApplicationRecord
  belongs_to :tweet
  belongs_to :user, foreign_key: :favorited_by_user_id
end
