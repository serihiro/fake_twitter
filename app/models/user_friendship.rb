# == Schema Information
#
# Table name: user_friendships
#
#  id           :integer          not null, primary key
#  from_user_id :integer          not null
#  to_user_id   :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_user_friendships_on_from_user_id_and_to_user_id  (from_user_id,to_user_id) UNIQUE
#

class UserFriendship < ApplicationRecord
  belongs_to :follow_from, class_name: 'User', foreign_key: :from_user_id
  belongs_to :follow_to, class_name: 'User', foreign_key: :to_user_id
end
