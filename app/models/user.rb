# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  user_name              :string(255)      not null
#  display_name           :string(255)      not null
#  description            :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_user_name             (user_name) UNIQUE
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :user_name, presence: true, length: { maximum: 20 },
    uniqueness: true, format: /\A[0-9a-zA-Z\-\_]+\Z/
  validates :display_name, presence: true, length: { maximum: 50 }
  validates :description, allow_nil: true, length: { maximum: 255 }
  validates :email, presence: true, uniqueness: true,
    length: { maximum: 255 }, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  has_many :follow_to, class_name: 'UserFriendship', foreign_key: :from_user_id, dependent: :destroy
  has_many :followings, class_name: 'User', through: :follow_to, source: :follow_to
  has_many :follow_from, class_name: 'UserFriendship', foreign_key: :to_user_id, dependent: :destroy
  has_many :followers, class_name: 'User', through: :follow_from, source: :follow_from
  has_many :tweets, dependent: :destroy
end
