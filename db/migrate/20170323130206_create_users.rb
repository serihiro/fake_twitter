class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :user_name, null: false
      t.string :display_name, null: false
      t.string :description
      t.timestamps
      t.index :user_name, unique: true
    end
  end
end
