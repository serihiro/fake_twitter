class CreateUserFriendships < ActiveRecord::Migration[5.1]
  def change
    create_table :user_friendships do |t|
      t.integer :from_user_id, null: false
      t.integer :to_user_id, null: false
      t.timestamps
      t.index %i(from_user_id to_user_id), unique: true
    end
  end
end
