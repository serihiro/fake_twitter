class CreateTweets < ActiveRecord::Migration[5.1]
  def change
    create_table :tweets do |t|
      t.string :tweet, null: false
      t.integer :user_id, null: false
      t.timestamps
      t.index :user_id
    end
  end
end
