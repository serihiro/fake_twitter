class CreateTweetFavorites < ActiveRecord::Migration[5.1]
  def change
    create_table :tweet_favorites do |t|
      t.references :tweet, index: true, null: false
      t.integer :favorited_by_user_id, null: false
      t.timestamps
      t.index :favorited_by_user_id
    end
  end
end
